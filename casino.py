import urllib3
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile


js_scroller = '''
(scroll_down = function (){
    window.scrollTo(0, document.body.scrollHeight);
    setTimeout(function() {
        window.scrollY != document.body.scrollHeight ? scroll_down() : 0;
    }, 2000);
})();
'''


def textmodeFirefox():
    firefoxProfile = FirefoxProfile()
    firefoxProfile.set_preference('permissions.default.stylesheet', 2)
    firefoxProfile.set_preference('permissions.default.image', 2)
    firefoxProfile.set_preference(
        'dom.ipc.plugins.enabled.libflashplayer.so',
        'false'
    )
    return webdriver.Firefox(firefoxProfile)


def parse_categories(root):
    result = {}
    for category in root.find_all("li", recursive=False):
        name  = next(category.stripped_strings)
        child = category.find("ul")

        url = category.find("a", recursive=False)["href"]
        result[name] = {
            "url"     : "http://www.mescoursescasino.fr" + url,
            "url_part": url.rsplit('/', 1)[1] if url != '#' else None,
        }

        if child:
            result[name]["sub_categories"] = parse_categories(child)
    return result


def get_categories(market_id):
    http = urllib3.PoolManager()
    casino_url = 'http://www.mescoursescasino.fr/ecommerce/GC-catalog/fr/{0}'
    response = http.request('GET', casino_url.format(market_id))

    if response.status != 200:
        print("requesting url failed with code: {0}".format(response.status))
        return None
    else:
        dom_tree = BeautifulSoup(response.data).find(id="navigation")
        return parse_categories(dom_tree)


def get_products_by_category(category_url):
    results = []
    driver = textmodeFirefox()
    driver.delete_all_cookies()
    driver.get(category_url)
    driver.execute_script(js_scroller)

    for product in driver.find_elements_by_xpath("//a[@itemprop='name']"):
        url = product.get_attribute('href')
        results.append({
            'url'  : url,
            'text' : product.text,
            'brand': product.find_element_by_tag_name('strong').text.strip(),
        })
    driver.quit()
    return results


def get_product_details(product):
    http = urllib3.PoolManager()
    response = http.request('GET', product.get('url'))

    if response.status != 200:
        print("requesting url failed with code: {0}".format(response.status))
        return None
    else:
        dom_img   = BeautifulSoup(response.data).find('a', {'class': 'zoom-img1'})
        dom_price = BeautifulSoup(response.data).find('div', {'itemprop': 'price'})
        product['details'] = {
            'price'       : dom_price.text.strip(),
            'image_href'  : dom_img.get('href'),
            'image_title' : dom_img.get('title'),
            'content_info': {}
        }

        for content_info in BeautifulSoup(response.data).findAll('div', {'class': 'cnt-info'}):
            content_parts = content_info.text.split(':')
            if len(content_parts) > 1:
                info_label = content_parts[0].strip().split('\n')[0]
                info_value = content_parts[1].strip()
                product['details']['content_info'][info_label] = info_value
        return product
