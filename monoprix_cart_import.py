from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile

DEFAULT_TIMEOUT = 5


def textmodeFirefox():
    firefoxProfile = FirefoxProfile()
    firefoxProfile.set_preference('permissions.default.stylesheet', 2)
    firefoxProfile.set_preference('permissions.default.image', 2)
    firefoxProfile.set_preference(
        'dom.ipc.plugins.enabled.libflashplayer.so',
        'false'
    )
    return webdriver.Firefox(firefoxProfile)


def cart_remove_product(driver, product):
    url = "http://courses.monoprix.fr/bemaxindex.displaybasketright.displayproductlistright.displayoneproductright:removeevent/{0}"
    driver.get(url.format(product.get("product")))


def cart_import(login, password):
    import_result = []
    driver = textmodeFirefox()
    driver.delete_all_cookies()
    driver.get("http://courses.monoprix.fr")
    if connect(driver, login, password).get('result') is True:
        dom_basket = driver.find_element_by_xpath("//div[@id='basketRightZone']")
        dom_products = dom_basket.find_elements_by_id("productRef")
        if len(dom_products) > 0:
            for product in dom_products:
                product_ref = product.get_attribute("value")
                dom_product_quantity = dom_basket.find_element_by_id("quantity"+product_ref)
                product_quantity = dom_product_quantity.get_attribute("value")
                import_result.append({
                    "product": product_ref,
                    "quantity": product_quantity
                })
        else:
            print(u"empty cart !!")
    else:
        print(u"login failed !!")

    driver.quit()
    return import_result


def find_by_id(driver, timeout, id):
    waiter = WebDriverWait(driver, timeout)
    return waiter.until(EC.presence_of_element_located((By.ID, id)))


def connect(driver, login, password):
    monoprix_cookie = {}
    connection_result = False
    try:
        dom_login = find_by_id(driver, DEFAULT_TIMEOUT, "username")
        dom_password = find_by_id(driver, DEFAULT_TIMEOUT, "password")
        dom_login.send_keys(login)
        dom_password.send_keys(password)
        dom_password.send_keys(Keys.RETURN)
        try:
            dom_password = find_by_id(driver, DEFAULT_TIMEOUT, "password")
            print(u"login failed, incorrect credentials")
        except Exception, e:
            monoprix_cookie = driver.get_cookie("www.monoprix.com")
            if monoprix_cookie is not None:
                connection_result = True
            else:
                print(u"can't find the auth cockie, assuming login failed")
    except Exception, e:
        print(u"login failed with exception: {0}".format(e.message))
    finally:
        return {"result": connection_result, "cookie": monoprix_cookie}
